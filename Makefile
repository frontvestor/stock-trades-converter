# Makefile
#

.PHONY: help

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

clean: build rm run ## Build, remove and run container

build: ## Build Docker image
	docker build --tag frontvestor/stock-trades-converter:latest .

run: ## Run container
	docker run --name stock-trades-converter --detach --publish 80:80 frontvestor/stock-trades-converter

rm: ## Remove container
	docker rm --force stock-trades-converter

login: ## Login to container
	docker exec -it stock-trades-converter /bin/sh

logs: ## Follow logs
	docker logs --follow stock-trades-converter

browse: ## Browse the app
	open http://localhost
