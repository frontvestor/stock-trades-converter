import csv

from tradesconverter.utils import dates


def is_valid_ssn(ssn):
    try:
        int(ssn)
        if len(ssn) is 12:
            return True
    except ValueError:
        return False


def is_valid_conversion_rate(conversion_rate):
    try:
        float(conversion_rate)
        return True
    except ValueError:
        return False


def get_sru_options(conversion_rate, ssn='195012310000'):
    return {
        'time': dates.get_timestamp('%H%M%S'),
        'date': dates.get_timestamp('%Y%m%d'),
        'year': dates.get_previous_year(),
        'ssn': ssn,
        'conversion_rate': float(conversion_rate),
    }


def generate_blanketter_sru(timestamp_filename, csv_filename, sru_options):
    sru_file = open(timestamp_filename, "x")
    with open(csv_filename) as csv_file:
        reader = csv.reader(csv_file)
        trades = get_trades(reader, sru_options)
        symbols = get_symbols_by_trades(trades)

        for symbol in symbols:
            sru_file.write('#BLANKETT K4-%sP4\n' % sru_options['year'])
            sru_file.write('#IDENTITET %s %s %s\n' % (sru_options['ssn'], sru_options['date'], sru_options['time']))

            sru_file.write('#UPPGIFT 3100 %s\n' % symbols[symbol]['Quantity'])
            sru_file.write('#UPPGIFT 3101 %s\n' % symbol)
            sru_file.write('#UPPGIFT 3102 %s\n' % symbols[symbol]['SellCost'])
            sru_file.write('#UPPGIFT 3103 %s\n' % symbols[symbol]['BuyCost'])
            sru_file.write('#UPPGIFT 3104 %s\n' % symbols[symbol]['Profit'])
            sru_file.write('#UPPGIFT 3105 %s\n' % symbols[symbol]['Loss'])

            sru_file.write('#BLANKETTSLUT\n')
        sru_file.write('#FIL_SLUT')

    sru_file.close()


def generate_csv(timestamp_filename, csv_filename, sru_options):
    file = open(timestamp_filename, "x")
    file.write('Symbol,Quantity,SellCost,BuyCost,Profit,Loss\n')

    with open(csv_filename) as csv_file:
        reader = csv.reader(csv_file)
        trades = get_trades(reader, sru_options)
        symbols = get_symbols_by_trades(trades)

        for symbol in symbols:
            file.write('%s,' % symbol)
            file.write('%s,' % symbols[symbol]['Quantity'])
            file.write('%s,' % symbols[symbol]['SellCost'])
            file.write('%s,' % symbols[symbol]['BuyCost'])
            file.write('%s,' % symbols[symbol]['Profit'])
            file.write('%s\n' % symbols[symbol]['Loss'])
    file.close()


def get_profit_and_loss(csv_filename, sru_options):

    with open(csv_filename) as csv_file:
        reader = csv.reader(csv_file)
        return get_pnl(reader, sru_options)


def get_pnl(reader, sru_options):
    pnl = 0
    trades = get_trades(reader, sru_options)
    for row in trades:
        pnl -= get_buying_price(row)
        pnl += get_selling_price(row)
    return pnl


def generate_info_sru(sru_filename, sru_options, form):
    sru_file = open(sru_filename, "x")

    sru_file.write('#DATABESKRIVNING_START\n')
    sru_file.write('#PRODUKT SRU\n')
    sru_file.write('#FILNAMN BLANKETTER.SRU\n')
    sru_file.write('#DATABESKRIVNING_SLUT\n')
    sru_file.write('#MEDIELEV_START\n')

    for key in sru_options:
        value = form[key]
        sru_file.write('#%s %s\n' % (sru_options[key], value))

    sru_file.write('#MEDIELEV_SLUT')

    sru_file.close()


def get_trades(reader, options):
    trades = []
    lines = 0
    for row in reader:
        if lines != 0:
            trades.append({
                'Symbol': row[0],
                'Type': row[1],
                'Quantity': abs(float(row[2])),
                'TradePrice': float(row[3]),
                'Commission': float(row[4]),
                'Currency': row[5],
                'ConversionRate': options['conversion_rate'],
            })
        lines += 1

    return trades


def get_symbols_by_trades(trades):
    symbols = {}
    for trade in trades:
        symbol = trade['Symbol']
        quantity = trade['Quantity']

        if symbol in symbols:
            previous_quantity = symbols[symbol]['Quantity']
            symbols[symbol]['Quantity'] = round((previous_quantity + quantity)/2)
        else:
            symbols[symbol] = {
                'Quantity': quantity
            }

        if trade['Type'] == 'SELL':
            symbols[symbol]['SellCost'] = get_selling_price(trade)

        if trade['Type'] == 'BUY':
            symbols[symbol]['BuyCost'] = get_buying_price(trade)

    for symbol in symbols:
        buy_cost = 0
        if 'BuyCost' in symbols[symbol]:
            buy_cost = symbols[symbol]['BuyCost']
        else:
            symbols[symbol]['BuyCost'] = 0

        sell_cost = 0
        if 'SellCost' in symbols[symbol]:
            sell_cost = symbols[symbol]['SellCost']
        else:
            symbols[symbol]['SellCost'] = 0

        difference = sell_cost - buy_cost
        if difference > 0:
            symbols[symbol]['Profit'] = difference
            symbols[symbol]['Loss'] = 0
        else:
            symbols[symbol]['Profit'] = 0
            symbols[symbol]['Loss'] = abs(difference)

    return symbols


def get_selling_price(row):
    price = row['Quantity'] * row['TradePrice'] - abs(row['Commission']) if row['Type'] == 'SELL' else 0
    return round(price * row['ConversionRate'])


def get_buying_price(row):
    price = row['Quantity'] * row['TradePrice'] + abs(row['Commission']) if row['Type'] == 'BUY' else 0
    return round(price * row['ConversionRate'])


def get_profit(row):
    return get_selling_price(row)


def get_loss(row):
    return get_buying_price(row)
