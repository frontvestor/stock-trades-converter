import unittest

from tradesconverter.utils import files


class UtilsFilesTest(unittest.TestCase):

    def test_allowed_file_should_pass_if_extension_is_allowed(self):
        result = files.allowed_file("test.csv")
        self.assertTrue(result)

    def test_allowed_file_should_fail_if_extension_is_not_allowed(self):
        result = files.allowed_file("test.mp3")
        self.assertFalse(result)


if __name__ == '__main__':
    unittest.main()
